# Dockers for ASE test configurations

## Dockers for use with test jobs

 * **anaconda**: This docker probably doesn't work, but it should support our
   anaconda pipeline ones someone fixes it.

 * **full-monty**: This docker includes as much software as possible in order
  to test as many things as possible.  This means calculator tests.
  It currently includes all those codes that we can install with apt.
  Probably we'll want different dockers to test custom builds, since those
  dockers will require more maintenance.

 * **main**: Our main test job, which includes most optional
   dependencies such as matplotlib, tkinter, and so on, and tests new
   versions of libraries.

 * **oldlibs**: Our oldlibs test job.  It tests the lowest-numbered versions
  of the libraries that we support.  It also has as few dependencies as
  possible, so we test whether ASE *can* work without those things.
  However: This also means that some features are not tested at all
  with lowest-version libraries.  (We could do better.)

There is a number of intermediate dockers as well which we use
mainly to establish full-monty in a sensible way.


## Hierarchy of dockers

The oldlibs and anaconda dockers live their own secluded, peaceful lives.
Enough about them.

The graph/ subdirectory can compile an svg visualization of the
other dockers' hierarchy.  Dockers are connected with arrows
when one is built from (i.e., FROM) another.  The arrow is grey
for multi-stage builds where only some files are copied over.

The full-monty docker is meant to have all the calculators, which is
a *lot*.  We don't want to dump it all into one big docker, because then
we need a lot of recompilation.  Therefore, we use a multi-stage build
with minimal dockers, one docker for each computational code.

All the computational codes must be compiled, so they all need certain
libraries and tools.  We hence establish a common base docker, derived
from python-xx-slim, on which we install gcc, gfortran, libxc, BLAS,
etc.  This docker is called **build-tools**.

From build-tools we establish minimal dockers for individual codes:
**dftbplus**, **octopus**, **siesta**, etc.  Each docker downloads
source code, unpacks, compiles, and installs into some directory.
That directory will later be copied when building the full-monty
docker, whereas other leftovers from download and compilation shall be
forgotten.

Some codes are available as debian packages and hence easy to install.
We do that from a separate image, **debian-calculators**, which has
abinit, cp2k, and perhaps others.

The **main** image is for testing ASE without calculators, but with
everything else (matplotlib, spglib, povray, database stuff, ...).
It also opportunistically installs gpaw and asap3 because those are
available on pypi but share some dependencies (notably numpy and scipy).
We base the main image on the debian-calculators image because
we expect the pip dependencies to change more frequently than the
debian dependencies, and also it seems to be practical.

Finally, the **full-monty** image is a multi-stage build which
copies the compiled program from each calculator-specific docker
(dftbplus, octopus, ...) into itself and otherwise is based on main.
This means it has everything installed.
