all-dockers: full-monty_

build-tools_:
	$(MAKE) -C build-tools

debian-calculators_: build-tools_
	$(MAKE) -C debian-calculators

main_: debian-calculators_
	$(MAKE) -C main

calculators_: main_
	$(MAKE) -C calculators

full-monty_: main_ calculators_
	$(MAKE) -C full-monty
